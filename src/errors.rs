use std::io::{self,ErrorKind};
use toml::de;

const NOT_FOUND: &str = "File not found";
const PERMISSION: &str = "You do not have permission";
const UTF8: &str = "File contains invalid UTF-8";

pub type Res<T, E = Error> = std::result::Result<T,E>;

#[derive(Debug)]
pub enum Error {
    Io(String),
    De(String),
    Fallback,
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        let io_err = |s: &str| Self::Io(s.to_owned());
        match e.kind() {
            ErrorKind::NotFound => io_err(NOT_FOUND),
            ErrorKind::PermissionDenied => io_err(PERMISSION),
            ErrorKind::InvalidData => io_err(UTF8),
            _ => Error::Fallback,
        }
    }
}

impl From<de::Error> for Error {
    fn from(e: de::Error) -> Self {
        let msg = format!("{}",e);
        Self::De(msg)
    }
}
