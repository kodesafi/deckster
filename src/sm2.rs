#[derive(Clone)]
pub enum Rating {
    Again,
    Good,
    Hard,
    Easy,
}

impl Rating {
    pub fn easiness(&self,e: f32) -> f32 {
        let e = match self {
            Self::Again => e - 0.2,
            Self::Hard => e - 0.15,
            Self::Good => e,
            Self::Easy => e + 0.15,
        };
        e.max(1.3)
    }
    pub fn days(&self, e: f32, d: f32) -> f32 {
        let d = match self {
            Self::Again => d * 0.5,
            Self::Hard => d * 1.2,
            Self::Good => d * e,
            Self::Easy => d * e * 1.3,
        };
        d.max(0.5)
    }
}
